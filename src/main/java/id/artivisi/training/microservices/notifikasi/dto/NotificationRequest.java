package id.artivisi.training.microservices.notifikasi.dto;

import lombok.Data;

@Data
public class NotificationRequest {
    private String noHp;
    private String email;
    private String message;
    private String title;
}
