package id.artivisi.training.microservices.notifikasi.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import id.artivisi.training.microservices.notifikasi.dto.NotificationRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import javax.management.remote.NotificationResult;

@Service
public class KafkaListenerService {

    private static final Logger LOGGER = LoggerFactory.getLogger(KafkaListenerService.class);

    @Autowired private ObjectMapper objectMapper;

    @KafkaListener(topics = "${kafka.topic.notification}")
    public void handleNotificationRequest(String msg) throws Exception {
        LOGGER.debug("Terima message : {}", msg);
        NotificationRequest nr = objectMapper.readValue(msg, NotificationRequest.class);
        LOGGER.debug("Object notifikasi : {}", nr);
    }
}
